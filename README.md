## docker-compose configuration

It includes the following:

##### - NGINX
  - config file : nginx.conf
  - Web Access: http://localhost:8080 or http://10.5.0.2
##### - PHP 7.1
  - code to be placed in "*code*" folder
  - Socket Access: 10.5.0.3:9000
##### - sendmail
##### - MailDev
  - All emails sent from PHP will be recieved and can be viewed here
  - Web Access http://localhost:8081 or http://10.5.0.6
##### - MySQL 5.7
  - credentials : root/root
  - Access: mysql -u root --host 10.5.0.4 --port 3306 -proot
##### - phpmyadmin
  - credentials : root/root
  - Web Access: http://localhost:8082 or http://10.5.0.5
##### - redis
  - Socket Access: 10.5.0.7:6379
  
================================================================


## Installation:

###### 1- Install docker

sudo apt-get update && sudo apt-get install curl 

curl -fsSL https://get.docker.com/ | sh

sudo service docker start

sudo usermod -aG docker {YOUR_UNIX_USERNAME}



###### 2- Install docker-compose

sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/bin/docker-compose

sudo chmod +x /usr/bin/docker-compose

###### 3- clone this repo into any folder

###### 4- docker-compose up

Step 4 could take upto 10 minutes


==================================================================


## Running Goldenscent Magento in docker:

1- import Goldenscent Database into the docker DB container

2- copy Magento code into "/code/goldenscent"

3- config app/etc/local.xml to use docker redis container  (10.5.0.7:6379)

4- config app/etc/local.xml to use docker database container  (10.5.0.4:3306)

5- config /etc/hosts , add the new line:

	10.5.0.2       local.goldenscent.com local.goldenscent.kw local.goldenscent.sa local.goldenscent.ae
    
6- Modify nginx.conf if needed

